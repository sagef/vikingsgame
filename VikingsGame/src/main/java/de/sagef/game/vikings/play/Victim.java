package de.sagef.game.vikings.play;

public class Victim {

    private int requiredAttacks;

    private int nextX;
    private int nextY;

    private int distance;
    private boolean inRange;

    public int getRequiredAttacks() {
        return requiredAttacks;
    }

    public void setRequiredAttacks(int requiredAttacks) {
        this.requiredAttacks = requiredAttacks;
    }

    public int getNextX() {
        return nextX;
    }

    public void setNextX(int nextX) {
        this.nextX = nextX;
    }

    public int getNextY() {
        return nextY;
    }

    public void setNextY(int nextY) {
        this.nextY = nextY;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public boolean isInRange() {
        return inRange;
    }

    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }

}
