package de.sagef.game.vikings.play;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

import com.google.gson.Gson;

import de.sagef.game.vikings.api.handler.VikingResponseHandler;
import de.sagef.game.vikings.api.handler.VikingWorldResponseHandler;
import de.sagef.game.vikings.api.model.Viking;
import de.sagef.game.vikings.api.model.VikingAction;
import de.sagef.game.vikings.api.model.VikingOrder;
import de.sagef.game.vikings.api.model.VikingPosition;
import de.sagef.game.vikings.api.model.VikingWorld;

public class VikingControl {

    private static final String SPAWN_DIR  = "viking-spawn";

    private HttpClient httpclient;
    private String url;
    private String heroName;
    private String heroId;

    public VikingControl(HttpClient httpclient, String url, String heroName) {
        this.httpclient = httpclient;
        this.url = url;
        this.heroName = heroName;
    }

    public Viking spawn() throws Exception {
        Gson gson = new Gson();

        VikingWorld world = scanWorld();
        boolean inWorld = world.getVikings().parallelStream().anyMatch(v -> heroName.equals(v.getName()));

        if (inWorld) {
            try {
                String heroContent = new String(Files.readAllBytes(Paths.get(SPAWN_DIR + "/" + heroName + ".json")));
                Viking fromFile = gson.fromJson(heroContent, Viking.class);
                heroId = fromFile.getId();
                return fromFile;
            }
            // not critical for game execution
            catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        }

        Viking request = new Viking();
        request.setName(heroName);

        StringEntity requestEntity = new StringEntity(gson.toJson(request), ContentType.APPLICATION_JSON);

        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(requestEntity);

        Viking response = httpclient.execute(httpPost, new VikingResponseHandler());
        heroId = response.getId();

        try {
            Path spawnDir = Paths.get(SPAWN_DIR);
            if (!Files.exists(spawnDir)) {
                Files.createDirectory(Paths.get(SPAWN_DIR));
            }

            Files.write(Paths.get(SPAWN_DIR + "/" + heroName + ".json"), gson.toJson(response).getBytes());
        }
        // not critical for game execution
        catch (IOException ex) {
            ex.printStackTrace();
        }

        return response;
    }

    public Viking move(int x, int y) throws Exception {
        return doAction(VikingAction.ORDER_MOVE, x, y);
    }

    public Viking attack(int x, int y) throws Exception {
        return doAction(VikingAction.ORDER_ATTACK, x, y);
    }

    public Viking heal() throws Exception {
        return doAction(VikingAction.ORDER_HEAL, 0, 0);
    }

    private Viking doAction(String order, int x, int y) throws Exception {
        Gson gson = new Gson();

        VikingPosition vikingPosition = new VikingPosition();
        vikingPosition.setX(x);
        vikingPosition.setY(y);

        VikingAction action = new VikingAction();
        action.setOrder(order);
        action.setPosition(vikingPosition);

        VikingOrder request = new VikingOrder();
        request.setId(heroId);
        request.setAction(action);

        StringEntity requestEntity = new StringEntity(gson.toJson(request), ContentType.APPLICATION_JSON);

        HttpPut httpPut = new HttpPut(url);
        httpPut.setEntity(requestEntity);

        return httpclient.execute(httpPut, new VikingResponseHandler());
    }

    public Viking getStatus() throws Exception {
        return heroId == null ? null : httpclient.execute(new HttpGet(url + "/" + heroId), new VikingResponseHandler());
    }

    public VikingWorld scanWorld() throws Exception {
        return httpclient.execute(new HttpGet(url), new VikingWorldResponseHandler());
    }

}
