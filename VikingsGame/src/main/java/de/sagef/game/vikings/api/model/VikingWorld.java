package de.sagef.game.vikings.api.model;

import java.util.List;

public class VikingWorld {

    private List<Viking> vikings;

    public List<Viking> getVikings() {
        return vikings;
    }

    public void setVikings(List<Viking> vikings) {
        this.vikings = vikings;
    }

}
