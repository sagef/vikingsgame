package de.sagef.game.vikings.api.handler;

public class VikingStatusCodeException extends RuntimeException {

    public VikingStatusCodeException(String message) {
        super(message);
    }

}
