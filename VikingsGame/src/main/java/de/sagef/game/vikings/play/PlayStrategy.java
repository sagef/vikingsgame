package de.sagef.game.vikings.play;

public interface PlayStrategy extends Runnable {

    void play() throws Exception;

}
