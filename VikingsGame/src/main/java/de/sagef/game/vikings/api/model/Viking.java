package de.sagef.game.vikings.api.model;

public class Viking {

    private String name;
    private int level;
    private int health;
    private int kills;
    private VikingPosition position;
    private String id;
    private VikingAction action;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public VikingPosition getPosition() {
        return position;
    }

    public void setPosition(VikingPosition position) {
        this.position = position;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VikingAction getAction() {
        return action;
    }

    public void setAction(VikingAction action) {
        this.action = action;
    }

}
