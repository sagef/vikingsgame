package de.sagef.game.vikings.api.model;

public class VikingOrder {

    private String id;
    private VikingAction action;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VikingAction getAction() {
        return action;
    }

    public void setAction(VikingAction action) {
        this.action = action;
    }

}
