package de.sagef.game.vikings;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;

import de.sagef.game.vikings.play.SimpleAttackPlayStrategy;
import de.sagef.game.vikings.play.VikingControl;

public class ValhallaMain {

    private final static String       DEFAULT_HOST         = "http://localhost:8080";
    private final static String       DEFAULT_PATH         = "/api/vikings";
    private final static String       DEFAULT_TEAM1_PREFIX = "Red";
    private final static String       DEFAULT_TEAM2_PREFIX = "Blue";
    private final static String       DEFAULT_VIKING_NAME  = "Ragnar";
    private final static int          DEFAULT_VIKINGS      = 1;
    private final static ValhallaMode DEFAULT_MODE         = ValhallaMode.BATLLE;

    public final static void main(String[] args) throws Exception {
        String host       = DEFAULT_HOST;
        String path       = DEFAULT_PATH;
        String baseName   = DEFAULT_VIKING_NAME;
        int vikings       = DEFAULT_VIKINGS;
        ValhallaMode mode = DEFAULT_MODE;

        if (args.length == 0) {
            System.out.println("INFO: possible call parameters:");
            System.out.println("<host> <viking name> <number of vikings> <one of BATLLE|TEAM|TEAMBATTLE>");
            System.out.println();
        }

        if (args.length > 0) {
            host = args[0];
        }
        if (args.length > 1) {
            baseName = args[1];
        }
        if (args.length > 2) {
            try {
                vikings = Integer.parseInt(args[2]);
            }
            catch (NumberFormatException e) {
            }
        }
        if (args.length > 3) {
            try {
                mode = ValhallaMode.valueOf(args[3].toUpperCase());
            }
            catch (IllegalArgumentException e) {
            }
        }

        System.out.println("INFO:");
        System.out.println("Verwendete Konfiguration:");
        System.out.println(host + " " + baseName + " " + vikings + " " + mode);
        System.out.println();

        HttpClient httpClient = HttpClients.createDefault();
        String url = host + path;

        ExecutorService pool = Executors.newFixedThreadPool(vikings);

        if (vikings == 1) {
            VikingControl vikingControl = new VikingControl(httpClient, url, baseName);
            pool.submit(new SimpleAttackPlayStrategy(vikingControl));
        }
        else {
            Set<String> teamNames = Collections.emptySet();
            Set<String> team1Names = new HashSet<>();
            Set<String> team2Names = new HashSet<>();

            for (int i = 1; i <= vikings; i++) {
                String vikingName = baseName + i;

                if (mode == ValhallaMode.TEAM) {
                    team1Names.add(vikingName);
                    teamNames = team1Names;
                }
                else if (mode == ValhallaMode.TEAMBATTLE) {
                    if (i % 2 == 0) {
                        vikingName = DEFAULT_TEAM1_PREFIX + baseName + (i / 2);
                        team1Names.add(vikingName);
                        teamNames = team1Names;
                    }
                    else {
                        vikingName = DEFAULT_TEAM2_PREFIX + baseName + (i / 2);
                        team2Names.add(vikingName);
                        teamNames = team2Names;
                    }
                }

                VikingControl vikingControl = new VikingControl(httpClient, url, vikingName);
                pool.submit(new SimpleAttackPlayStrategy(vikingControl, teamNames));
            }
        }

        pool.shutdown();
    }

}
