package de.sagef.game.vikings.api.handler;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import de.sagef.game.vikings.api.model.Viking;

public class VikingResponseHandler implements ResponseHandler<Viking> {

    @Override
    public Viking handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
        int status = response.getStatusLine().getStatusCode();
        if (status != 200 && status != 400) {
            throw new VikingStatusCodeException("Http Status not supported: " + status);
        }

        HttpEntity entity = response.getEntity();
        if (entity == null) {
            return null;
        }

        String json = EntityUtils.toString(entity, "UTF-8");
        return json != null ? new Gson().fromJson(json, Viking.class) : null;
    }

}
