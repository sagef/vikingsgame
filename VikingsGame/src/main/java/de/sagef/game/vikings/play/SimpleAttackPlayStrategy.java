package de.sagef.game.vikings.play;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.apache.http.conn.HttpHostConnectException;

import de.sagef.game.vikings.api.handler.VikingStatusCodeException;
import de.sagef.game.vikings.api.model.Viking;
import de.sagef.game.vikings.api.model.VikingPosition;
import de.sagef.game.vikings.api.model.VikingWorld;

public class SimpleAttackPlayStrategy implements PlayStrategy {

    private VikingControl vikingControl;
    private Viking myViking;
    private Set<String> teamNames = Collections.emptySet();

    public SimpleAttackPlayStrategy(VikingControl vikingControl) {
        this.vikingControl = vikingControl;
    }

    public SimpleAttackPlayStrategy(VikingControl vikingControl, Set<String> teamNames) {
        this.vikingControl = vikingControl;
        this.teamNames = teamNames;
    }

    @Override
    public void run() {
        try {
            play();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void play() throws Exception {
        while (true) {
            try {
                myViking = vikingControl.getStatus();
                if (myViking == null || myViking.getHealth() == 0) {
                    myViking = vikingControl.spawn();
                }

                if (myViking == null) {
                    Thread.sleep(1000);
                    continue;
                }

                VikingWorld world = vikingControl.scanWorld();
                Optional<Victim> potentialVictim = findVictim(world.getVikings());
                if (!potentialVictim.isPresent()) {
                    healOnMissingHealth();
                    continue;
                }

                Victim victim = potentialVictim.get();
                if (!victim.isInRange()) {
                    healOnMissingHealth();
                    vikingControl.move(victim.getNextX(), victim.getNextY());
                    continue;
                }

                for (int i = 0; i < victim.getRequiredAttacks(); i++) {
                    vikingControl.attack(victim.getNextX(), victim.getNextY());
                }
            }
            catch (HttpHostConnectException | VikingStatusCodeException e) {
                e.printStackTrace();
                break;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void healOnMissingHealth() throws Exception {
        if (myViking.getHealth() < myViking.getLevel() * 2) {
            int requiredHeals = (int) Math.ceil(myViking.getHealth() / (myViking.getLevel() * 2.0));
            for (int i = 0; i < requiredHeals; i++) {
                vikingControl.heal();
            }
        }
    }

    private Optional<Victim> findVictim(List<Viking> list) {
        return list
                .parallelStream()
                .filter(v -> !myViking.getName().equals(v.getName()))
                .filter(v -> !teamNames.contains(v.getName()))
//              .filter(v -> myViking.getLevel() >= v.getLevel())
                .map(enemyViking -> {
                    Viking[] ret = new Viking[2];
                    ret[0] = myViking;
                    ret[1] = enemyViking;
                    return ret;
                })
                .map(vikingToVictimPosition)
                .sorted((p1, p2) -> Integer.compare(p1.getDistance(), p2.getDistance()))
                .findFirst();
    }

    private static Function<Viking[], Victim> vikingToVictimPosition = (tupel) -> {
        Viking me = tupel[0];
        Viking enemy = tupel[1];
        VikingPosition myPostion = me.getPosition();
        VikingPosition enemyPostion = enemy.getPosition();

        int requiredAttacks = (int) Math.ceil(enemy.getHealth() / (me.getLevel() * 1.0));
        int diffX = myPostion.getX() - enemyPostion.getX();
        int diffY = myPostion.getY() - enemyPostion.getY();
        int nextX = (diffX > 0) ? -1 : ((diffX < 0) ? 1 : 0);
        int nextY = (diffY > 0) ? -1 : ((diffY < 0) ? 1 : 0);
        int distance = Math.max(Math.abs(diffX), Math.abs(diffY));
        boolean inRange = distance == 1;

        Victim victim = new Victim();
        victim.setRequiredAttacks(requiredAttacks);
        victim.setNextX(nextX);
        victim.setNextY(nextY);
        victim.setDistance(distance);
        victim.setInRange(inRange);

        return victim;
    };

}
