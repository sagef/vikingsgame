package de.sagef.game.vikings.api.model;

public class VikingAction {

    public static final String ORDER_HEAL   = "heal";
    public static final String ORDER_MOVE   = "move";
    public static final String ORDER_ATTACK = "attack";

    private String order;
    private VikingPosition position;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public VikingPosition getPosition() {
        return position;
    }

    public void setPosition(VikingPosition position) {
        this.position = position;
    }

}
